Rails.application.routes.draw do
  root 'welcome#index'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    '/likes',   to: 'artciles#show_liked_article'
    resources :users do
    member do
      get :following, :followers
    end
  end
  resources :users
  resources :article_likes
  resources :artciles, only: [:create, :show, :destroy] do 
    member do
      resources :article_likes, only: [:create], as: 'article_likes_omar'
    end
  end
  resources :relationships, only: [:create, :destroy]
end
