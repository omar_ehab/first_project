class ArticleLike < ApplicationRecord
  belongs_to :article, class_name: "Artcile"
  belongs_to :user
  validates :article_id, presence: true
  validates :user_id, presence: true
end
