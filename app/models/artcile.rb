class Artcile < ApplicationRecord
  belongs_to :user
  has_many :article_likes
  has_many :likes, through: :article_likes
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :contant, presence: true
  validates :title, presence: true
end
