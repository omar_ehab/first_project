class ArtcilesController < ApplicationController
  before_action :set_article, only: [:show]
  before_action :logged_in_user, only: [:create, :show, :destroy]

  #create new artcile
  def create
    @artciles = current_user.artciles.build(artcile_params)
    if @artciles.save
      flash[:success] = "Artcile created!"
      redirect_to root_url
    else
      render 'welcome/index'
    end
  end
  
  #show all fav artciles
  def show_liked_article
    @articles = current_user.likes
    render 'artciles/likes'
  end

private

    #get artcile paramiters
    def artcile_params
      params.require(:artcile).permit(:title, :contant)
    end
    
    #find artcile by id
    def set_article
      @artciles = Artcile.find(params[:id])
    end
end
