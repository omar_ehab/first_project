class WelcomeController < ApplicationController
  def index
    if logged_in?
      @artcile = current_user.artciles.build if logged_in?
      @feed_items = current_user.feed.paginate(page: params[:page])
    else
      redirect_to login_url
    end
  end
  
end
