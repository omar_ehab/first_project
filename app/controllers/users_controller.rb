class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy,
    :following, :followers]
  
  #show user profile and articles
  def show
    @user = User.find(params[:id])
    @artciles = @user.artciles.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end

   #create new user
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Welcome to the Articles App!"
      redirect_to @user
    else
      render 'new'
    end
  end


  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  #fav artciles
  def liked_articles
    @title = "Best Articles"
    @artciles = current_user.likes.paginate(page: params[:page])
    render 'show_best_articles'
  end
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
end
