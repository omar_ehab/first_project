class ArticleLikesController < ApplicationController
  before_action :logged_in_user
  before_action :set_article_like, only: [:destroy]

  #create like
  def create
    @article = Artcile.find(params[:id])
    current_user.like(@article)
    redirect_to @article
  end

  #destroy like
  def destroy
    if @article_like.user == current_user
      current_user.unlike(@article_like.article)
    end
    redirect_to @article_like.article
  end

  private
    #fine liked artciles
    def set_article_like
      @article_like = ArticleLike.find(params[:id])
    end
end
