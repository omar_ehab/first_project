class EditLikes < ActiveRecord::Migration[5.2]
  def change
    add_index :article_likes, :article_id
    add_index :article_likes, :user_id
    add_index :article_likes, [:article_id, :user_id], unique: true
  end
end
