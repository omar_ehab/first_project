class CreateArticleLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :article_likes do |t|
      t.integer :article_id
      t.integer :user_id

      t.timestamps
    end
    add_index :article_likes, :article_id
    add_index :article_likes, :user_id
    add_index :article_likes, [:article_id, :user_id], unique: true
  end
end
