class CreateArtciles < ActiveRecord::Migration[5.2]
  def change
    create_table :artciles do |t|
      t.text :contant
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_column :artciles, :title, :string
    add_index :artciles, [:user_id, :created_at]
  end
end
